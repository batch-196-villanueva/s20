console.log("Hello World!");

let number = parseInt(prompt("Enter number:"));
console.log("The number you have provided is " + number);

for(i=number; i > 0; i--){
	if(i <= 50){
		console.log("The current value is at " + i + ". Terminating the loop.");
		break;
	}

	else if(i%10 === 0){
		console.log("The current value is divisible by 10. Skipping the number...");
		continue;
	}
	else if(i%5 === 0){
		console.log(i);
		continue;
	}
};

function multiplicationTable(num){
	for(i=1; i <= 10; i++){
		let product = num*i;
		console.log(num + " * " + i + " = " + product);
	};
};

console.log("Multiplication Table");
multiplicationTable(5);